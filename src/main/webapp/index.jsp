<%@page contentType="text/html" pageEncoding="UTF-8"%>
<anyxmlelement xmlns:c="http://java.sun.com/jsp/jstl/core" />
<!DOCTYPE html>
<html>
<head>
<title>Processed files</title>
<meta charset="UTF-8">
</head>
<body>
    <a href="JobExecution">Processed files:</a>
    <table>
        <%
        String[] processedFiles = request.getParameterValues("processedFiles");
        if (processedFiles != null) {

            for (int i = 0; i < processedFiles.length; i++) {
                out.println("<tr>");
                out.println("<td>");

                out.println(processedFiles[i]);
                out.println("</td>");
                out.println("</tr>");
            }
        }
        %>
    </table>
</body>
</html>