package quartz;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/*
 * Executable job for file compression and copying
 */
@WebServlet(name = "JobExecution", urlPatterns = { "/" })
public class JobExecution extends HttpServlet implements Job {
    private static final long serialVersionUID = 1L;

    /*
     * Compresses every file from readFromFolder and copies it in saveZipToFolder, deleting source file
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // Directories for files to process
        String readFromFolder = "src/main/resources/";
        String saveZipToFolder = "src/main/resources/zipped/";

        File directoryPath = new File(readFromFolder);

        List<File> files = new ArrayList<File>(Arrays.asList(directoryPath.listFiles()));
        Iterator<File> iterator = files.iterator();
        List<String> removedFilePaths = new ArrayList<String>();
        
        while (iterator.hasNext()) {
            File currentFile = iterator.next(); // must be called before iterator.remove()
            if (!currentFile.isDirectory()) {
                // Zip file
                Compress.zipFile(currentFile.getAbsolutePath());

                // Copy zipped file to destination folder
                File zippedFile = new File(currentFile.getName() + ".zip");
                File saveZipDirectoryPath = new File(saveZipToFolder + "/" + zippedFile.getName());
                try {
                    // Check copy option - threads
                    Files.copy(zippedFile.toPath(), saveZipDirectoryPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Remove iterator for safe file deletion
                iterator.remove();
                
                removedFilePaths.add(currentFile.toString());
                
                // Delete source file 
                currentFile.delete();
            }
        }

        System.out.println("Doing job execution");
    }
    
    /*
     * Failed method.
     */
    @Override
    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) 
                         throws IOException, ServletException {

        List<String> removedFilePaths = new ArrayList<String>();
        removedFilePaths.add("test.txt");
        request.setAttribute("processedFiles", removedFilePaths);
        
        getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
