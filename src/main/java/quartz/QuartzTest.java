package quartz;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/*
 * Job scheduler 
 */
public class QuartzTest {

    /*
     * Creates job scheduler with 10sec spooling interval. Job from {@link quartz.JobExecution class}
     */
    public static void main(String[] args) {

        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();

            JobDetail job = newJob(JobExecution.class).withIdentity("transfer").build();
            
            // Job spooling interval
            Integer interval = 10;

            SimpleTrigger trigger = newTrigger().withIdentity("trigger1").startNow()
                    .withSchedule(simpleSchedule().withIntervalInSeconds(interval).repeatForever()).build();
            
            scheduler.scheduleJob(job, trigger);
            
//            scheduler.shutdown();

        } catch (SchedulerException se) {
            se.printStackTrace();
        }
    }
}